# ravegrunt sites

This builds [ravegrunt.com](https://ravegrunt.com) and [ravegrunt.is](https://ravegrunt.is) using Hugo and GitLab pages. 

## Gitpod

Hugo Extended, which the [Hugo Coder theme](https://github.com/luizdepra/hugo-coder) requires, can't be installed with [brew](https://brew.sh) and instead needs to be installed via go with a flag.

This means that in the prebuild (or `init`) phase, it clones and installs it. 

In addition, it runs the command to load up the site so I can start cracking - I don't need to wait for Hugo to get running to start messing with my `config.toml` or whatever content I am working on.
I rather enjoy just slapping the button below and start working pretty much anywhere I have a browser, even my iPad or Windows machine.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/nalthis/ravegrunt-sites)

## Analytics

I want to know more about people who visit this site, but I am tin-foil-hat levels of privacy concerned.
With that in mind, I use [Fathom Analytics (referral link)](https://usefathom.com/ref/VJEEET).
They are specifically designed to be able to track things that are compliant to GDPR without needing a cookies notice, because they don't use a cookie at all.
[Really](https://usefathom.com/compliance).

Check out the [dashboard for this site](https://app.usefathom.com/share/babueycd/ravegrunt) to get a feel for what they track.

(Note: I leave on tracking while coding, so you will see a lot of `*.gitpod.io` in there)

## GitLab CI

In order to use GitLab pages well, I use a `.gitlab-ci.yml` that builds to a specific, pinned version of Hugo extended (currently `0.91.2`).
It builds the page on all non-main branches and builds and deploys only on main.
The .gitlab-ci.yml isn't too different than others I have [written about](https://nalth.is/rockstar-development/).
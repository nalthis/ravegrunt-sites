+++
title = "About"
description = "Marty Henderson"
aliases = ["about-us", "about-marty", "contact"]
author = "Marty Henderson"
+++

Marty Henderson is a professional consultant who works on platform engineering and tooling to improve developer experience.

Marty has worked with many clients over the years including those in finance, health care, and government, everything from pre-seed startups to Fortune 10 organizations.
His experience doesn't stop at the US border, having worked on issues with Europe's GDPR and Australia's APP laws.
Primarily focused on delivering a platform experience for developers and optimizing workflow, Marty has been known to deal with the most difficult of technical problems and directly for the executive suite.


He maintains a blog at https://nalth.is giving technical information and insight into the workings of improving the lives of developers for the non-technical.

You can reach him at:

Matrix: @ravegrunt:beeper.com  
Email: marty@ravegrunt.com
